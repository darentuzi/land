const makePlugin = require('ilp-plugin')
const crypto = require('crypto')
const IlpPacket = require('ilp-packet')
const ILDCP = require('ilp-protocol-ildcp')
const {Writer} = require('oer-utils')
const asymClient = require('ilp-plugin-xrp-asym-client')
//const myPlugin = require('ilp-plugin-btp')
// export ILP_CREDENTIALS="{\"address\":\"r3D6Rbp6h8fRYYfbStL7bC4CnqxYedGNUW\",\"secret\":\"shhMjjezqyEaizhtAPEjP59JBBinu\",\"server\":\"btp+ws://daren:tuzi@localhost:7002\"}"
// export ILP_CREDENTIALS="{\"address\":\"r3D6Rbp6h8fRYYfbStL7bC4CnqxYedGNUW\",\"secret\":\"shhMjjezqyEaizhtAPEjP59JBBinu\",\"server\":\"btp+ws://daren:tuzi@localhost:6666\"}"
// export ILP_PLUGIN="ilp-plugin-xrp-asym-client"

const credentials = {
    xrpServer: 'wss://s.altnet.rippletest.net:51233',
    address: "r3D6Rbp6h8fRYYfbStL7bC4CnqxYedGNUW",
    secret: "shhMjjezqyEaizhtAPEjP59JBBinu",
    server: "btp+ws://daren:tuzi@localhost:7002",
    claimInterval: 5 * 60 * 1000,
    assetCode: "XRP",
    assetScale: 6,
    balance: {
        minimum: "-Infinity",
        maximum: "20000000",
        settleThreshold: "5000000",
        settleTo: "10000000"
    },
    sendRoutes: true,
    receiveRoutes: true,
};

const httplugin = {
    multi: true,
    ildcp: {
        clientAddress: 'test.map',
        assetCode: 'XRP',
        assetScale: 9
    },
    incoming: {
        port: 6666,

        staticToken: 'pEe+DBOmULfMbX53WDEOf0z73ONNFisNL7KRwio3nFY=', // (required if using simple)

        // JWT authentication
        jwtSecret: 'pEe+DBOmULfMbX53WDEOf0z73ONNFisNL7KRwio3nFY=' // (required if using JWTs)
    },
    outgoing: { // (required) describes outgoing http calls
        url: 'http://localhost:7003',//http://explore.dev.equilibrium.co/peers/land/

        // Simple bearer authentication
        staticToken: 'pEe+DBOmULfMbX53WDEOf0z73ONNFisNL7KRwio3nFY=', // (required if using simple)

        // JWT authentication
        jwtSecret: 'pEe+DBOmULfMbX53WDEOf0z73ONNFisNL7KRwio3nFY=', // (required if using JWTs)
        jwtExpiry: 10 * 1000, // how often to sign a new token for auth

        http2: false, // whether `url` uses http2
        name: 'map' // name to send in `ILP-Peer-Name` header, for ilp addr.
    }
};

class Ping {
    constructor(deps) {
        this.conditionMap = new Map()
        this.plugin = new asymClient(credentials)
        //this.plugin = makePlugin();
    }

    async init() {
        console.log('connecting ping')
        try {
            await this.plugin.connect();
        } catch (err) {
            console.log(err);
        }
        console.log('ping connected.')

        this.plugin.registerDataHandler(data => {
            const {executionCondition} = IlpPacket.deserializeIlpPrepare(data)

            const fulfillment = this.conditionMap.get(executionCondition.toString('base64'))
            if (fulfillment) {
                this.conditionMap.delete(executionCondition.toString('base64'))
                return IlpPacket.serializeIlpFulfill({
                    fulfillment: fulfillment,
                    data: Buffer.alloc(0)
                })
            } else {
                throw new Error('unexpected packet.')
            }
        })
    }

    async ping(destination) {
        console.log('PINGING DESTINATION: ', destination)
        const fulfillment = crypto.randomBytes(32)
        const condition = crypto.createHash('sha256').update(fulfillment).digest()
        const {clientAddress} = await ILDCP.fetch(this.plugin.sendData.bind(this.plugin))

        console.log("here");
        console.log(clientAddress);
        console.log(condition);
        console.log(fulfillment);

        this.conditionMap.set(condition.toString('base64'), fulfillment)

        const writer = new Writer()

        writer.write(Buffer.from('ECHOECHOECHOECHO', 'ascii'))
        writer.writeUInt8(0)
        writer.writeVarOctetString(Buffer.from(clientAddress, 'ascii'))

        const result = await this.plugin.sendData(IlpPacket.serializeIlpPrepare({
            destination,
            amount: '100',
            executionCondition: condition,
            expiresAt: new Date(Date.now() + 30000),
            data: writer.getBuffer()
        }))

        const parsedPacket = IlpPacket.deserializeIlpPacket(result)
        console.log(parsedPacket);
        if (parsedPacket.type !== IlpPacket.Type.TYPE_ILP_FULFILL) {
            console.log('parsedPacket: ', parsedPacket)
            console.log('Error sending ping. code=' + parsedPacket.data.code + ' message=' + parsedPacket.data.message)
        }

        console.log('parsedPacket: ', parsedPacket)
    }
}

module.exports = Ping
